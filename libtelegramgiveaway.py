#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Copyright (C) 2018 Altispeed Technologies
Authored by Simon Quigley <simon.quigley@altispeed.com>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

A copy of the GNU General Public License version 3 is in LICENSE.
"""

# FIXME: Clean up these includes...
import sys
import random
from telethon.sync import TelegramClient
from telethon import functions, types
from telethon.tl.types import ChannelParticipantsSearch

# These are not actual values, and should be replaced.
api_id = 123456
api_hash = "HASH"

def getparticipants(channel, limit=None):
    with TelegramClient("Ask Noah Show Giveaway", api_id, api_hash) as client:
            # Telegram won't let us get more than 200 members at a time, so let's get
            # them bit by bit.
            offset = 0
            limit = 100
            all_participants = []

            while True:
                participants = client(functions.channels.GetParticipantsRequest(
                    channel=channel,
                    offset=offset,
                    limit=limit,
                    filter=ChannelParticipantsSearch(''),
                    hash=0
                ))
                if not participants.users:
                    break
                all_participants.extend(participants.users)
                offset += len(participants.users)

    return all_participants

def getuser(participants, specificuser=None):
    if specificuser == None:
        # This makes sure that we're not picking a bot.
        while True:
            user = random.choice(participants)
            if user.bot == False:
                break
        chosenuser = "The chosen user is: "
    else:
        try:
            user = participants[specificuser-1]
            chosenuser = "User "+str(specificuser)+" is: "
        except IndexError:
            return "There is no user "+str(specificuser)+" quite yet."
            exit(1)

    # The reason why we don't grab these values indiscriminately is because
    # some users don't have non-empty usernames or last names set in Telegram,
    # and they still should be able to win the giveaway.
    if user.first_name:
        chosenuser = chosenuser+user.first_name+" "

    if user.last_name:
        chosenuser = chosenuser+user.last_name+" "

    if user.username:
        chosenuser = chosenuser+"("+user.username+")"

    return chosenuser

def main():
    participants = getparticipants("asknoahshow")
    print(getuser(participants))

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        print("\nExiting...")
